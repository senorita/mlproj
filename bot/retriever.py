from pathlib import Path
from typing import List

import attr

from bot.knn.index import EmbeddingIndex
from bot.models.embedder import EmbedderConfig, Embedder
from common.entities import Document
from db.client import DocumentIndex


@attr.s(auto_attribs=True)
class RetrieverConfig:
    index_address: str
    embedder_config: EmbedderConfig
    emb_index_path: Path
    emb_index_vector_sz: int
    top_k: int = 30


class Retriever:
    def __init__(self, config: RetrieverConfig):
        self.config = config
        self.embedder = Embedder(config.embedder_config)
        self.emb_index = EmbeddingIndex(config.emb_index_vector_sz)
        self.emb_index.deserialize_from(config.emb_index_path)
        self.index = DocumentIndex(config.index_address)

    def retrieve(self, question: str) -> List[Document]:
        query_vecs = self.embedder.embed_question(question).numpy()
        doc_ids = self.emb_index.search_knn(query_vecs, self.config.top_k)[0][0]
        docs = self.index.get_docs(doc_ids)
        return docs
