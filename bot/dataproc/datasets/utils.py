from typing import List

from bot.dataproc.datasets import DatasetConfig, AnswererInputInstance, DatasetType, bioasq, sciq


def read_dataset(config: DatasetConfig) -> List[AnswererInputInstance]:
    instances = []
    if config.data_type == DatasetType.BIOASQ:
        instances = bioasq.read_bioasq(config.input_files)
    elif config.data_type == DatasetType.SCIQ:
        instances = sciq.read_sciq(config.input_files)
    else:
        raise Exception(f'unknown datatype: {config.data_type}')

    return instances