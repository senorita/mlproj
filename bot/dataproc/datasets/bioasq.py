import json
import re
from pathlib import Path
from typing import List, Any, Dict

from bot.dataproc.datasets import AnswererInputInstance
from common.utils import normalize


def read_bioasq(paths: List[Path]) -> List[AnswererInputInstance]:
    raw_qs: list = []
    for p in paths:
        raw_qs.extend(json.loads(p.read_text())['questions'])
    print("Read", len(raw_qs), "questions")
    instances = convert_json_to_instances(raw_qs)
    print("Created", len(instances), "input instances")
    return instances


def convert_json_to_instances(raw_qs: List[Dict[str, Any]]) -> List[AnswererInputInstance]:
    instances = []
    for raw_q in raw_qs:
        if raw_q['type'] != 'factoid':
            continue
        q = normalize(raw_q['body'])

        for a in raw_q['exact_answer']:
            if type(a) == list:
                a = a[0]
            assert type(a) == str
            a = normalize(a)
            for s_id, s in enumerate(raw_q['snippets']):
                context = normalize(s['text'])
                matches = re.finditer(re.escape(a), context)
                for m_id, m in enumerate(matches):
                    q_id = f'{raw_q["id"]}_{s_id}_{m_id}'
                    a_start = m.start()
                    instances.append(AnswererInputInstance(
                        id=q_id,
                        question=q,
                        context=context,
                        answer=a,
                        answer_start=a_start
                    ))
    return instances
