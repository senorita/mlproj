import json
import re
from pathlib import Path
from typing import List, Dict, Any

from bot.dataproc.datasets import AnswererInputInstance
from common.utils import normalize


def read_sciq(paths: List[Path]) -> List[AnswererInputInstance]:
    raw_qs = []
    for p in paths:
        raw_qs.extend(json.loads(p.read_text()))
    print("Read", len(raw_qs), "questions")
    instances = convert_json_to_instances(raw_qs)
    print("Created", len(instances), "input instances")
    return instances


def convert_json_to_instances(raw_qs: List[Dict[str, Any]]) -> List[AnswererInputInstance]:
    instances = []
    for q_id, raw_q in enumerate(raw_qs):
        if not raw_q['support']:
            continue
        q = normalize(raw_q['question'])
        context = normalize(raw_q['support'])
        answer = normalize(raw_q['correct_answer'])

        matches = re.finditer(re.escape(answer.lower()), context.lower())
        for m_id, m in enumerate(matches):
            orig_match = context[m.start(): m.end()]
            instances.append(AnswererInputInstance(
                id=f'{q_id}_{m_id}',
                question=q,
                context=context,
                answer=orig_match,
                answer_start=m.start(),
            ))
        # import pdb; pdb.set_trace()

    return instances
