from enum import Enum
from pathlib import Path
from typing import Optional, List

import attr


@attr.s(auto_attribs=True)
class AnswererInputInstance:
    id: str
    question: str
    context: str
    answer: Optional[str] = attr.ib(default=None)
    answer_start: Optional[int] = attr.ib(default=None)


class DatasetType(Enum):
    BIOASQ = 'bioasq'
    SCIQ = 'sciq'
    SQUAD = 'squad'


@attr.s(auto_attribs=True)
class DatasetConfig:
    input_files: List[Path]
    data_type: DatasetType
