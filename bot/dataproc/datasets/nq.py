import json
from pathlib import Path
from typing import List, Optional

import attr


@attr.s(auto_attribs=True)
class RerankerExample:
    question: str
    context: str
    label: Optional[int] = None
    q_idx: Optional[str] = None
    c_idx: Optional[str] = None


def read_nq(path: Path) -> List[RerankerExample]:
    data = json.loads(path.read_text())
    instances: List[RerankerExample] = []
    for d in data:
        q = d['question']
        for ctx in d['positive_ctxs'][:5]:
            instances.append(RerankerExample(
                question=q,
                context=ctx['text'],
                label=1
            ))
        for ctx in d['negative_ctxs'][:5]:
            instances.append(RerankerExample(
                question=q,
                context=ctx['text'],
                label=0
            ))
    print("Collected", len(instances), "instances")
    return instances
