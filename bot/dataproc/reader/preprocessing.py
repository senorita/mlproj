from typing import List, NamedTuple

import attr
from torch.utils.data import Dataset, TensorDataset
from transformers import SquadExample, AutoTokenizer, AutoConfig, squad_convert_examples_to_features, SquadFeatures

from bot.dataproc.datasets import AnswererInputInstance


@attr.s(auto_attribs=True)
class QaInputFeatures:
    pass


class Preprocessor:
    def __init__(self, model_name: str):
        pretr_config = AutoConfig.from_pretrained(model_name)
        self.tokenizer = AutoTokenizer.from_pretrained(model_name, use_fast=False)
        self.max_seq_length = 384
        self.doc_stride = 128
        self.max_query_length = 32
        self.padding_strategy = "max_length"


class ModelFeatureSet(NamedTuple):
    examples: List[SquadExample]
    features: List[SquadFeatures]
    dataset: Dataset


class SquadDataset(Dataset):
    def __init__(self, features: TensorDataset, is_train: bool):
        self.features = features
        self.is_train = is_train

    def __getitem__(self, item):
        feature = self.features[item]
        return {
            'input_ids': feature[0],
            'attention_mask': feature[1],
            'token_type_ids': feature[2],
            'start_positions': feature[3],
            'end_positions': feature[4],
        }

    def __len__(self):
        return len(self.features)


def make_features(instances: List[AnswererInputInstance], preprocessor: Preprocessor, is_training: bool) -> ModelFeatureSet:
    examples = convert_to_squad_examples(instances)
    features, dataset = squad_convert_examples_to_features(
        examples=examples,
        tokenizer=preprocessor.tokenizer,
        max_seq_length=preprocessor.max_seq_length,
        doc_stride=preprocessor.doc_stride,
        max_query_length=preprocessor.max_query_length,
        is_training=is_training,
        threads=8,
        return_dataset="pt",
    )
    return ModelFeatureSet(examples, features, SquadDataset(dataset, is_training))


def convert_to_squad_examples(instances: List[AnswererInputInstance]) -> List[SquadExample]:
    examples: List[SquadExample] = []
    for i in instances:
        examples.append(SquadExample(
            qas_id=i.id,
            question_text=i.question,
            context_text=i.context,
            answer_text=i.answer,
            start_position_character=i.answer_start,
            title='',
        ))
    return examples
