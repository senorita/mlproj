from typing import List, NamedTuple, Dict, Optional

import torch
from torch import Tensor
from torch.utils.data import Dataset
from transformers import AutoTokenizer
from transformers.tokenization_utils_base import PaddingStrategy, TruncationStrategy

from bot.dataproc.datasets.nq import RerankerExample


class RerankerInput(NamedTuple):
    input_ids: Tensor
    token_type_ids: Tensor
    attention_mask: Tensor
    label: Optional[Tensor] = None
    q_id: Optional[str] = None
    c_id: Optional[str] = None


class ListDataset(Dataset[Dict]):
    def __init__(self, items: List[RerankerInput]):
        self.items = items

    def __getitem__(self, index: int) -> Dict:
        res = self.items[index]._asdict()
        res = {k: v for k, v in res.items() if v is not None}
        return res

    def __len__(self):
        return len(self.items)


class RerankerPreprocessor:
    def __init__(self, pretr_name: str, max_length: int, train: bool):
        self.tokenizer = AutoTokenizer.from_pretrained(pretr_name)
        self.max_length = max_length
        self.train = train

    def reranker_features(self, examples: List[RerankerExample]) -> Dataset[Dict]:
        items = [self.encode_example(e) for e in examples]
        return ListDataset(items)

    def encode_example(self, e: RerankerExample) -> RerankerInput:
        res = self.tokenizer.__call__(
            text=e.question,
            text_pair=e.context,
            max_length=self.max_length,
            padding=PaddingStrategy.MAX_LENGTH,
            truncation=TruncationStrategy.ONLY_SECOND,
            return_tensors='pt',
        )

        assert not self.train or e.label is not None
        return RerankerInput(
            input_ids=res['input_ids'][0],
            token_type_ids=res['token_type_ids'][0],
            attention_mask=res['attention_mask'][0],
            label=torch.scalar_tensor(e.label, dtype=torch.long) if self.train else None,
            q_id=e.q_idx,
            c_id=e.c_idx,
        )
