from pathlib import Path
from typing import List, Optional

import attr
import torch
import transformers
from torch import Tensor
from torch.utils.data import DataLoader

from bot.dataproc.datasets.nq import RerankerExample
from bot.dataproc.reranker.preprocessing import RerankerPreprocessor
from common.entities import Document
from common.utils import choose_device, batch_to_device


@attr.s(auto_attribs=True)
class RerankerConfig:
    model_dir: Optional[Path] = None
    pretr_name: str = 'bert-base-uncased'
    max_length: int = 384
    top_k: int = 10


def init_reranker_components(config: RerankerConfig):
    assert config.model_dir
    model_config = transformers.models.bert.BertConfig.from_pretrained(str(config.model_dir), num_labels=2)
    model = transformers.models.bert.BertForSequenceClassification.from_pretrained(str(config.model_dir), config=model_config)
    return model


class Reranker:
    def __init__(self, config: RerankerConfig):
        self.config = config
        self.device = choose_device(True)
        self.model = init_reranker_components(config)
        self.model.to(self.device)
        self.preprocessor = RerankerPreprocessor(config.pretr_name, config.max_length, False)

    def rerank(self, question: str, documents: List[Document]):
        examples: List[RerankerExample] = []
        for d in documents:
            examples.append(RerankerExample(
                question=question,
                context=d.text,
                c_idx=d.id,
            ))

        insts = self.preprocessor.reranker_features(examples=examples)
        loader = DataLoader(insts, batch_size=16)

        all_out: Optional[Tensor] = None
        self.model.eval()
        with torch.no_grad():
            for b_idx, batch in enumerate(loader):
                batch = batch_to_device(batch, self.device)
                batch = {k: v for k, v in batch.items() if k in ['input_ids', 'token_type_ids', 'attention_mask']}
                out = self.model(**batch)
                if all_out is None:
                    all_out = out.logits
                else:
                    all_out = torch.cat([all_out, out.logits])

        assert all_out is not None
        probs = torch.softmax(all_out, dim=1)[:, 1].cpu().numpy()

        docs = [d.with_score(s) for d, s in zip(documents, probs)]
        docs = sorted(docs, key=lambda d: d.score, reverse=True)
        return docs[:self.config.top_k]
