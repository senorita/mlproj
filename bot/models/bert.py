from enum import Enum
from pathlib import Path
from typing import List, Dict, Optional

import attr
import torch
from torch.utils.data import DataLoader
from transformers import AutoModelForQuestionAnswering
from transformers.data.processors.squad import SquadResult

from bot.dataproc.datasets import AnswererInputInstance
from bot.dataproc.reader.postprocessing import compute_predictions_logits
from bot.dataproc.reader.preprocessing import make_features, Preprocessor
from common.entities import Document, Answer, Span
from common.utils import choose_device, batch_to_device


class Algorithm(Enum):
    BERT_SQUAD = 'bert_squad'


@attr.s(auto_attribs=True)
class AnswererConfig:
    use_cuda: bool
    algorithm: Algorithm
    algorithm_dir: Optional[Path] = None


def init_algorithm(a: Algorithm, alg_dir: Optional[Path] = None):
    if a == Algorithm.BERT_SQUAD:
        model = str(alg_dir)
        return AutoModelForQuestionAnswering.from_pretrained(model), Preprocessor(model)
    else:
        raise


def to_list(tensor):
    return tensor.detach().cpu().tolist()


@attr.s(auto_attribs=True)
class RawAnswer:
    text: str
    start: int
    probability: float


@attr.s(auto_attribs=True)
class AnswererOutput:
    doc2anses: Dict[str, List[RawAnswer]]


class Answerer:
    def __init__(self, config: AnswererConfig):
        self.config = config
        self.device = choose_device(config.use_cuda)
        self.model, self.preprocessor = init_algorithm(config.algorithm, config.algorithm_dir)
        self.model.to(self.device)

    def answer(self, documents: List[Document], question: str, size: int = 5) -> List[Answer]:
        instances = []
        for d in documents:
            instances.append(AnswererInputInstance(
                id=d.id,
                question=question,
                context=d.text,
            ))
        out = self.infer(instances)

        doc_dict = {d.id: d for d in documents}
        answer_dict = {}
        for doc_id, anses in out.doc2anses.items():
            for a in anses:
                if not a.text or a.text == 'empty':
                    break
                ans_text = a.text.lower().strip()
                if ans_text not in answer_dict:
                    answer_dict[ans_text] = Answer(ans_text, [])
                # todo: Merge by docid.
                answer_dict[ans_text].evidence.append(
                    Span(start=a.start, end=a.start + len(a.text), context=doc_dict[doc_id], score=a.probability)
                )

        answers: List[Answer] = list(answer_dict.values())
        answers = sorted(answers, key=lambda x: sum(e.score for e in x.evidence), reverse=True)[:size]
        return answers

    def infer(self, instances: List[AnswererInputInstance]) -> AnswererOutput:
        examples, features, dataset = make_features(instances, self.preprocessor)

        self.model.eval()
        with torch.no_grad():
            loader = DataLoader(dataset, batch_size=32)
            all_results = []
            for batch_idx, batch in enumerate(loader):
                feature_indices = batch[3]
                batch = dict(zip(['input_ids', 'attention_mask', 'token_type_ids'], batch))
                batch = batch_to_device(batch, self.device)

                outputs = self.model(**batch)

                for i, feature_index in enumerate(feature_indices):
                    eval_feature = features[feature_index.item()]
                    unique_id = int(eval_feature.unique_id)

                    output = [to_list(output[i]) for output in outputs.to_tuple()]
                    start_logits, end_logits = output
                    result = SquadResult(unique_id, start_logits, end_logits)

                    all_results.append(result)

        preds = compute_predictions_logits(
            all_examples=examples,
            all_features=features,
            all_results=all_results,
            n_best_size=5,
            max_answer_length=30,
            do_lower_case=False,
            output_prediction_file=None,
            output_nbest_file=None,
            output_null_log_odds_file=None,
            verbose_logging=False,
            version_2_with_negative=False,
            null_score_diff_threshold=0.0,
            tokenizer=self.preprocessor.tokenizer,
        )

        out = AnswererOutput(doc2anses={})
        # todo: dont forget about questions
        for d_id, anses in preds.items():
            out.doc2anses[d_id] = []
            for a in anses:
                out.doc2anses[d_id].append(RawAnswer(
                    text=a['text'],
                    start=a['start_tok'],
                    probability=a['probability'])
                )
        return out
