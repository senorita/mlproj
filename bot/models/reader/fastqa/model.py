from typing import List, Optional

import attr
import torch
from torch import nn, Tensor

from bot.models.reader.fastqa.config import FastQAConfig


@attr.s(auto_attribs=True)
class FastQAOutput:
    start_logits: Tensor
    loss: Optional[Tensor]


class FastQA(nn.Module):
    def __init__(self, config: FastQAConfig, word_embs: Tensor, char_embs: Tensor):
        super().__init__()
        self.config = config
        self.word_embs = nn.Embedding.from_pretrained(word_embs, padding_idx=1)
        self.char_embs = nn.Embedding.from_pretrained(char_embs, padding_idx=1)

        self.lstm = nn.LSTM(
            # input_size=self.word_embs.embedding_dim + self.char_embs.embedding_dim + 2,
            input_size=self.word_embs.embedding_dim,
            hidden_size=config.hidden_size,
            num_layers=config.rnn_num_layers,
            bidirectional=True,
        )

        self.b_q = nn.Linear(config.hidden_size * 2, config.hidden_size)
        # self.b_q.load_state_dict({
        #     'weight': torch.cat([torch.eye(config.hidden_size), torch.eye(config.hidden_size)]),
        #     **self.b_q.state_dict()
        # })
        self.b_c = nn.Linear(config.hidden_size * 2, config.hidden_size)

        self.v_q = nn.Linear(config.hidden_size, 1)

        self.ans_start_linear = nn.Linear(config.hidden_size * 2, config.hidden_size)
        self.ans_start_linear_q = nn.Linear(config.hidden_size, config.hidden_size)
        self.ans_start_linear_distr = nn.Linear(config.hidden_size, 1)

        self.ce_loss = nn.CrossEntropyLoss()

    def embed(self, word_ids, char_ids=None):
        word_emb = self.word_embs.forward(word_ids)
        # for c in char_ids:
        #     char_emb = self.char_embs(c)
        return word_emb

    #
    def forward(
            self,
            q_word_ids: Tensor,
            q_word_count: List[int],
            # q_char_ids: Tensor,
            c_word_ids: Tensor,
            c_word_count: List[int],
            # c_char_ids: Tensor,
            a_start_pos: Optional[Tensor] = None,
            a_end_pos: Optional[Tensor] = None,
    ):
        x_q = self.embed(q_word_ids)
        x_c = self.embed(c_word_ids)

        # LSTM
        z, _ = self.lstm.forward(x_q)
        h, _ = self.lstm.forward(x_c)

        z = torch.tanh(self.b_q(z))
        h = torch.tanh(self.b_c(h))

        # z_hat
        alpha = torch.softmax(self.v_q(z), 1)
        z_hat = alpha.mul(z).sum(1)

        # answer start
        a_inp = torch.cat([z_hat.unsqueeze(1) * h, h], 2)
        a = torch.relu(self.ans_start_linear(a_inp))
        # a = torch.relu(self.ans_start_linear(a_inp) + self.ans_start_linear_q(z_hat).unsqueeze(1))
        a = self.ans_start_linear_distr(a).squeeze(2)
        start_logits = torch.softmax(a, 1)

        # answer end
        a_end_inp = torch.cat([h, z_hat.unsqueeze(1), h * z_hat.unsqueeze(1)], 2)
        a_end = torch.relu(self.ans_start_linear(a_end_inp))
        a_end = self.ans_start_linear_distr(a_end).squeeze(2)
        end_logits = torch.softmax(a_end, 1)

        loss = None
        if a_start_pos is not None:
            loss = self.ce_loss.forward(start_logits, a_start_pos)
            loss += self.ce_loss.forward(end_logits, a_end_pos)

        return loss, start_logits, end_logits
        # return FastQAOutput(
        #     start_logits=a,
        #     loss=loss,
        # )
