import collections
import itertools
import multiprocessing as mp
from pathlib import Path
from typing import Dict, List, Tuple

import numpy as np
import torch
import tqdm
from torch import Tensor
from torch.utils.data import Dataset

from bot import AnswererInputInstance
from bot.models.reader.fastqa.config import FastQAConfig

UNK_TOKEN = '<unk>'
PAD_TOKEN = '<pad>'
EOS_TOKEN = '<eos>'
SOS_TOKEN = '<sos>'


class Dictionary:
    def __init__(self, embeddings: Tensor, word2emb_id: Dict[str, int]):
        self.embeddings: Tensor = embeddings
        self.word2emb_id: Dict[str, int] = word2emb_id

    def embed(self, s: str) -> Tensor:
        return self.embeddings[self.word2emb_id[s]]

    def embed_or_unk(self, s: str):
        if self.has(s):
            return self.embed(s)
        return self.embeddings[self.unk_id]

    def encode(self, s: str):
        return self.word2emb_id[s] if self.has(s) else self.unk_id

    def encode_list(self, s: List[str]):
        return [self.encode(_s) for _s in s]

    def has(self, s):
        return s in self.word2emb_id

    @property
    def pad_id(self):
        return self.word2emb_id[PAD_TOKEN]

    @property
    def unk_id(self):
        return self.word2emb_id[UNK_TOKEN]

    @property
    def eos_id(self):
        return self.word2emb_id[EOS_TOKEN]

    @property
    def sos_id(self):
        return self.word2emb_id[SOS_TOKEN]


def read_emb_line(line: str):
    raw_emb = line.split(' ')
    assert len(raw_emb) == 301
    word = raw_emb[0]
    emb = [float(n) for n in raw_emb[1:]]
    return word, emb


def read_embs(embs_path: Path, word_emb_dim: int = 300, char_emb_dim: int = 100) -> Tuple[Dictionary, Dictionary]:
    embs = [
        np.random.normal(size=word_emb_dim).tolist(),
        np.random.normal(size=word_emb_dim).tolist(),
        np.random.normal(size=word_emb_dim).tolist(),
        np.random.normal(size=word_emb_dim).tolist(),
    ]
    word2id = {UNK_TOKEN: 0, PAD_TOKEN: 1, EOS_TOKEN: 2, SOS_TOKEN: 3}
    char2id = {UNK_TOKEN: 0, PAD_TOKEN: 1}
    with embs_path.open(encoding='utf-8') as f:
        with mp.Pool() as pool:
            f = itertools.islice(f)
            res = list(pool.imap(read_emb_line, tqdm.tqdm(f), chunksize=500))
    char_counter = collections.Counter()
    for r in res:
        word, emb = r
        word2id[word] = len(embs)
        embs.append(emb)
        char_counter.update(word)
    final_embs = torch.tensor(embs)

    for ch, count in char_counter.most_common():
        if count < 1:
            break
        char2id[ch] = len(char2id)

    return (
        Dictionary(embeddings=final_embs, word2emb_id=word2id),
        Dictionary(embeddings=torch.normal(0, 0.1, (len(char2id), char_emb_dim)), word2emb_id=char2id)
    )


class Tokenizer:
    def __init__(self, config: FastQAConfig, word_dict: Dictionary, char_dict: Dictionary):
        self.word_dict = word_dict
        self.char_dict = char_dict
        self.max_cseq_len = config.max_cseq_len
        self.max_qseq_len = config.max_qseq_len
        self.max_word_len = config.max_word_len

    @staticmethod
    def tokenize(s):
        return s.strip().lower().split()

    def encode(self, s: str, is_q: bool):
        seq_len = self.max_qseq_len if is_q else self.max_cseq_len
        prefix = 'q_' if is_q else 'c_'

        tokens = self.tokenize(s)
        word_ids = []
        char_word_ids = []

        word_ids.append(self.word_dict.sos_id)
        for t in tokens[:seq_len - 2]:
            word_id = self.word_dict.encode(t)
            word_ids.append(word_id)
            char_ids = self.char_dict.encode_list(list(t))
            char_word_ids.append(char_ids[:self.max_word_len])
        word_ids.append(self.word_dict.eos_id)

        word_len = len(word_ids)

        word_ids = word_ids + [self.word_dict.pad_id] * (seq_len - len(word_ids))
        assert len(word_ids) == seq_len

        return {
            prefix + 'word_ids': word_ids,
            prefix + 'word_count': word_len,
            # prefix + 'char_ids': char_word_ids,
        }

    def encode_pair(self, q: str, s: str):
        q_inp = self.encode(q, is_q=True)
        c_inp = self.encode(s, is_q=False)
        return {
            **q_inp,
            **c_inp,
        }

    def encode_with_answer(self, q: str, s: str, ans_start: int, ans: str):
        assert s[ans_start:].startswith(ans), (s, ans_start, ans)
        res = self.encode_pair(q, s)

        tok_start = len(self.tokenize(s[:ans_start]))
        tok_end = len(self.tokenize(s[:ans_start + len(ans)]))
        assert self.tokenize(s)[tok_start: tok_end] == self.tokenize(ans)

        if tok_end > self.max_cseq_len:
            tok_start, tok_end = 0, 0
        return {
            'a_start_pos': tok_start,
            'a_end_pos': tok_end,
            **res,
        }


def make_features(examples: List[AnswererInputInstance], tokenizer: Tokenizer) -> List[Dict]:
    features = []
    for e in examples:
        if e.answer_start is not None:
            feature = tokenizer.encode_with_answer(e.question, e.context, e.answer_start, e.answer)
        else:
            feature = tokenizer.encode_pair(e.question, e.context)
        features.append(feature)
    return features


class RNNDataset(Dataset):
    def __init__(self, items: List[Dict]):
        self.items = items

    def __getitem__(self, index) -> Dict:
        # return self.items[index]
        return {k: torch.tensor(v) for k, v in self.items[index].items()}

    def __len__(self):
        return len(self.items)


def pack_seq(word_ids):
    word_count = [len(w_ids) for w_ids in word_ids]
    word_ids = torch.nn.utils.rnn.pad_sequence(word_ids, batch_first=True, padding_value=1)
    return word_ids, word_count
