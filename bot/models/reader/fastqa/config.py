import json

import attr
from cattr import unstructure


@attr.s(auto_attribs=True)
class FastQAConfig:
    hidden_size: int = 300
    rnn_num_layers: int = 3

    max_qseq_len: int = 100
    max_cseq_len: int = 100
    max_word_len: int = 10

    def to_json_string(self):
        return json.dumps(unstructure(self))
