import attr


@attr.s(auto_attribs=True)
class TransformerConfig:
    pretr_model_name: str
