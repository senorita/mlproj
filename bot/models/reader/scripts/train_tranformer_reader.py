import argparse
import os
import sys
from pathlib import Path
from typing import Dict

import attr
import datasets
from transformers import AutoModelForQuestionAnswering, TrainingArguments, EvalPrediction
from transformers.data.processors.squad import SquadResult
from transformers.trainer import Trainer

sys.path.append(os.path.join(os.path.dirname(__file__), '../../../../'))

from bot.dataproc.reader.postprocessing import compute_predictions_logits
from bot.dataproc.datasets import DatasetConfig
from bot.dataproc.datasets.utils import read_dataset
from bot.dataproc.reader.preprocessing import make_features, Preprocessor, ModelFeatureSet
from bot.models.reader.tranformer.config import TransformerConfig
from common.utils import config_from_file, set_seed


@attr.s(auto_attribs=True)
class TrainConfig:
    output_dir: Path

    train_data_config: DatasetConfig
    eval_data_config: DatasetConfig

    tf_config: TransformerConfig

    num_epochs: int = 2
    lr: float = 2e-5
    warmup: float = 0.1
    per_gpu_batch_size: int = 16

    seed: int = 0

    local_rank: int = -1


class MetricComputer:
    def __init__(self, preprocessor: Preprocessor):
        self.preprocessor = preprocessor
        self.squad_metrics = datasets.load_metric('squad')

    def compute(self, pred: EvalPrediction, eval_data: ModelFeatureSet) -> Dict:
        all_res = []
        for s, e, f in zip(*pred.predictions, eval_data.features):
            all_res.append(SquadResult(f.unique_id, start_logits=s, end_logits=e))
        pred_texts = compute_predictions_logits(
            all_examples=eval_data.examples,
            all_features=eval_data.features,
            all_results=all_res,
            n_best_size=5,
            max_answer_length=30,
            do_lower_case=False,
            output_prediction_file=None,
            output_nbest_file=None,
            output_null_log_odds_file=None,
            verbose_logging=False,
            version_2_with_negative=False,
            null_score_diff_threshold=0.0,
            tokenizer=self.preprocessor.tokenizer,
        )
        formatted_predictions = [{"id": k, "prediction_text": v[0]['text']} for k, v in pred_texts.items()]
        references = [{"id": ex.qas_id, "answers": {'text': [ex.answer_text], 'answer_start': [ex.start_position]}} for ex in eval_data.examples]

        compute = {}
        try:
            compute = self.squad_metrics.compute(predictions=formatted_predictions, references=references)
        except:
            import pdb;
            pdb.set_trace()
        print("Metrics:", compute)
        return compute


def is_main_process(rank):
    return rank == 0 or rank == -1


def main(args: TrainConfig):
    if is_main_process(args.local_rank):
        print("Running training with args:", args)
    set_seed(args.seed)

    train_args = TrainingArguments(
        output_dir=str(args.output_dir),

        learning_rate=args.lr,
        per_device_train_batch_size=args.per_gpu_batch_size,
        num_train_epochs=args.num_epochs,

        logging_steps=10,
        evaluation_strategy='epoch',
        # eval_steps=1,

        # warmup_steps=500,
        fp16=True,

        seed=args.seed,
        local_rank=args.local_rank
    )

    train_instances = read_dataset(args.train_data_config)
    eval_instances = read_dataset(args.eval_data_config)

    # eval_instances = train_instances
    # train_instances = train_instances[:100]
    # eval_instances = eval_instances[:100]

    preprocessor = Preprocessor(args.tf_config.pretr_model_name)

    train_data = make_features(train_instances, preprocessor, True)
    eval_data = make_features(eval_instances, preprocessor, False)

    model = AutoModelForQuestionAnswering.from_pretrained(args.tf_config.pretr_model_name)

    metric_computer = MetricComputer(preprocessor)

    num_steps = len(train_data.features) * args.num_epochs // args.per_gpu_batch_size
    num_warmup_steps = int(args.warmup * num_steps)
    train_args.warmup_steps = num_warmup_steps

    trainer = Trainer(
        model=model,
        args=train_args,
        train_dataset=train_data.dataset,
        eval_dataset=eval_data.dataset,
        compute_metrics=lambda p: metric_computer.compute(p, eval_data),
    )
    trainer.evaluate()
    trainer.train()


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--config_from_file', type=config_from_file(TrainConfig))
    parser.add_argument('--local_rank', type=int, default=-1)
    raw_args = parser.parse_args()
    conf: TrainConfig = raw_args.config_from_file
    conf.local_rank = raw_args.local_rank
    main(conf)
