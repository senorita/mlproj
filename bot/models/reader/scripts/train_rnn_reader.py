import argparse
import os
import sys
from pathlib import Path
from typing import NamedTuple, List, Dict

import attr
from torch.utils.data import Dataset
from transformers import TrainingArguments, SchedulerType
from transformers.trainer import Trainer

sys.path.append(os.path.join(os.path.dirname(__file__), '../../../../'))

from bot.dataproc.datasets.common import DatasetConfig, AnswererInputInstance
from bot.dataproc.datasets.utils import read_dataset
from bot.models.reader.fastqa.model import FastQA
from bot.models.reader.fastqa.data import read_embs, Tokenizer, make_features, RNNDataset
from bot.models.reader.fastqa.config import FastQAConfig
from common.utils import config_from_file, set_seed


@attr.s(auto_attribs=True)
class TrainConfig:
    output_dir: Path

    train_data_config: DatasetConfig
    eval_data_config: DatasetConfig

    word_embs_path: Path
    word_embs_dim: int = 300
    char_embs_dim: int = 100
    fastqa_config: FastQAConfig = FastQAConfig()

    seed = 0


class ModelFeatureSet(NamedTuple):
    examples: List[AnswererInputInstance]
    features: List[Dict]
    dataset: Dataset


def prepare_dataset(config: DatasetConfig, tokenizer: Tokenizer) -> ModelFeatureSet:
    examples = read_dataset(config)
    features = make_features(examples, tokenizer)
    dataset = RNNDataset(features)
    return ModelFeatureSet(examples=examples, features=features, dataset=dataset)


def main(args: TrainConfig):
    set_seed(args.seed)

    word_dict, char_dict = read_embs(args.word_embs_path, args.word_embs_dim, args.char_embs_dim)

    tokenizer = Tokenizer(args.fastqa_config, word_dict, char_dict)

    # examples = [
    #     AnswererInputInstance('0', 'who who', 'also when', answer_start=5, answer='when'),
    #     AnswererInputInstance('1', 'what', 'there where', answer_start=6, answer='where'),
    # ]

    train_data = prepare_dataset(args.train_data_config, tokenizer)
    eval_data = prepare_dataset(args.eval_data_config, tokenizer)

    model = FastQA(args.fastqa_config, word_dict.embeddings, char_dict.embeddings)

    train_args = TrainingArguments(
        output_dir=str(args.output_dir),
        learning_rate=1e-3,
        per_device_train_batch_size=32,
        num_train_epochs=20,
        logging_first_step=True,
        logging_steps=1,
        lr_scheduler_type=SchedulerType.CONSTANT,
    )
    trainer = Trainer(
        args=train_args,
        model=model,
        train_dataset=train_data.dataset,
        eval_dataset=eval_data.dataset,
    )

    trainer.train()
    trainer.save_model()


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--config_from_file', type=config_from_file(TrainConfig))
    main(parser.parse_args().config_from_file)
