import argparse
import glob
import os
import sys

sys.path.append(os.path.join(os.path.dirname(__file__), '../../'))

from bot.knn.index import EmbeddingIndex


def main(args: argparse.Namespace):
    index = EmbeddingIndex(args.vector_size, args.index_buffer)

    ctx_files_pattern = args.embs_files_pattern
    input_paths = sorted(glob.glob(ctx_files_pattern))

    index.train(input_paths, 100_000)
    print('Reading all passages data from files: %s', input_paths)

    index.index_data(input_paths)

    index_path = "_".join(input_paths[0].split("_")[:-1])
    index.serialize(index_path)
    print('Wrote index to', index_path)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--vector_size', type=int, default=768)
    parser.add_argument('--index_buffer', type=int, default=50_000)
    parser.add_argument('--embs_files_pattern', type=str)
    main(parser.parse_args())
