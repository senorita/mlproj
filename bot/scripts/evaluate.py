import argparse
import json
from pathlib import Path
from typing import List

import attr
from cattr import Converter

from bot.dataproc.datasets import AnswererInputInstance, DatasetType
from bot.dataproc.datasets import bioasq, sciq
from bot.models import Answerer, AnswererConfig


@attr.s(auto_attribs=True)
class Arguments:
    eval_input: List[Path]
    eval_input_type: DatasetType
    answerer_config: AnswererConfig


def main(args: Arguments):
    instances: List[AnswererInputInstance]
    if args.eval_input_type == DatasetType.BIOASQ:
        instances = bioasq.read_bioasq(args.eval_input)
    elif args.eval_input_type == DatasetType.SCIQ:
        instances = sciq.read_sciq(args.eval_input)
    else:
        print('kek')
        return

    answerer = Answerer(args.answerer_config)
    answerer.infer(instances)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    converter = Converter()
    converter.register_structure_hook(Path, lambda x, _: Path(x).expanduser())
    # parser.add_argument('args', type=lambda x: converter.structure(json.loads(x), Arguments))
    # args = parser.parse_args().args
    parser.add_argument('--args', type=lambda x: (json.loads(x)), default=None)
    parser.add_argument('--args_from_path', type=lambda x: (json.loads(Path(x).expanduser().read_text())), default=None)

    parsed = parser.parse_args()
    raw_args = None
    if parsed.args:
        raw_args = parsed.args
    elif parsed.args_from_path:
        raw_args = parsed.args_from_path
    else:
        print("No args provided")
        exit(1)

    args = converter.structure(raw_args, Arguments)
    main(args)
