import argparse
import os
import sys
from pathlib import Path

import attr
import transformers
from transformers import TrainingArguments
from transformers.trainer import Trainer

sys.path.append(os.path.join(os.path.dirname(__file__), '../../'))

from bot.dataproc.datasets.nq import read_nq
from bot.dataproc.reranker.preprocessing import RerankerPreprocessor
from bot.models.reranker import RerankerConfig
from common.utils import structure, json_text


@attr.s(auto_attribs=True)
class RerankerTrainConfig:
    output_dir: Path
    log_dir: Path

    train_path: Path
    eval_path: Path = None
    reranker_config: RerankerConfig = RerankerConfig()

    lr: float = 2e-5
    epochs: int = 2
    bsize: int = 16

    seed: int = 0
    local_rank: int = -1


def main(config: RerankerTrainConfig):
    args = TrainingArguments(
        output_dir=str(config.output_dir),
        overwrite_output_dir=True,

        learning_rate=config.lr,
        num_train_epochs=config.epochs,
        per_device_train_batch_size=config.bsize,
        warmup_steps=500,
        fp16=True,

        seed=config.seed,
        local_rank=config.local_rank,

        logging_first_step=True,
        logging_steps=10
    )

    preprocessor = RerankerPreprocessor(config.reranker_config.pretr_name, config.reranker_config.max_length, True)

    train_dataset = preprocessor.reranker_features(read_nq(config.train_path)[:])
    eval_dataset = preprocessor.reranker_features(read_nq(config.eval_path)[:])
    # eval_dataset = train_dataset

    model_config = transformers.models.bert.BertConfig.from_pretrained(config.reranker_config.pretr_name, num_labels=2)
    model = transformers.models.bert.BertForSequenceClassification.from_pretrained(config.reranker_config.pretr_name, config=model_config)
    trainer = Trainer(
        model=model,
        args=args,
        train_dataset=train_dataset,
        eval_dataset=eval_dataset,
    )

    trainer.train()
    trainer.save_model()


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--config_from_path', type=json_text, required=True)
    parser.add_argument("--local_rank", type=int, default=-1)
    raw_args = parser.parse_args()
    args: RerankerTrainConfig = structure(raw_args.config_from_path, RerankerTrainConfig)

    args.local_rank = raw_args.local_rank
    print("Rank:", args.local_rank)
    main(args)
