import argparse
import os
import sys
from pathlib import Path
from typing import Dict

import attr
import torch
import torch.nn.functional as F
import transformers
from transformers import TrainingArguments, EvalPrediction
from transformers.trainer import Trainer

sys.path.append(os.path.join(os.path.dirname(__file__), '../../'))

from bot.dataproc.datasets.nq import read_nq
from bot.dataproc.reranker.preprocessing import RerankerPreprocessor
from bot.models.reranker import RerankerConfig
from common.utils import structure, json_text


@attr.s(auto_attribs=True)
class RerankerEvalConfig:
    output_dir: Path

    eval_path: Path = None
    reranker_config: RerankerConfig = RerankerConfig()

    seed: int = 0


def compute_metrics(pred: EvalPrediction) -> Dict:
    eps = 1e-5

    y_true = F.one_hot(torch.tensor(pred.label_ids), 2).to(torch.float32)
    y_pred = F.softmax(torch.tensor(pred.predictions, dtype=torch.float32), dim=1)

    tp = (y_true * y_pred).sum(dim=0).to(torch.float32)
    tn = ((1 - y_true) * (1 - y_pred)).sum(dim=0).to(torch.float32)
    fp = ((1 - y_true) * y_pred).sum(dim=0).to(torch.float32)
    fn = (y_true * (1 - y_pred)).sum(dim=0).to(torch.float32)

    precision = tp / (tp + fp + eps)
    recall = tp / (tp + fn + eps)

    f1 = 2 * (precision * recall) / (precision + recall + eps)
    f1 = f1.clamp(min=eps, max=1 - eps)

    return {
        'f1': f1.mean().item(),
        'prec': precision.mean().item(),
        'rec': recall.mean().item(),
    }


def main(config: RerankerEvalConfig):
    args = TrainingArguments(
        output_dir=str(config.output_dir),

        fp16=True,

        seed=config.seed,
    )

    preprocessor = RerankerPreprocessor(config.reranker_config.pretr_name, config.reranker_config.max_length, True)

    eval_dataset = preprocessor.reranker_features(read_nq(config.eval_path))
    assert config.output_dir
    model_config = transformers.models.bert.BertConfig.from_pretrained(config.output_dir, num_labels=2)
    model = transformers.models.bert.BertForSequenceClassification.from_pretrained(config.output_dir, config=model_config)
    trainer = Trainer(
        model=model,
        args=args,
        eval_dataset=eval_dataset,
        compute_metrics=compute_metrics
    )

    trainer.evaluate()


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--config_from_path', type=json_text, required=True)
    parser.add_argument("--local_rank", type=int, default=-1)
    raw_args = parser.parse_args()
    args: RerankerEvalConfig = structure(raw_args.config_from_path, RerankerEvalConfig)

    args.local_rank = raw_args.local_rank
    print("Rank:", args.local_rank)
    main(args)
