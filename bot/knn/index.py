import logging
import os
import pickle
import time
from pathlib import Path
from typing import List, Tuple, Iterator

import faiss
import numpy as np
import tqdm

logger = logging.getLogger()


def iterate_encoded_files(vector_files: list) -> Iterator[Tuple[object, np.array]]:
    for i, file in enumerate(vector_files):
        logger.info('Reading file %s', file)
        with open(file, "rb") as reader:
            doc_vectors = pickle.load(reader)
            for doc in doc_vectors:
                db_id, doc_vector = doc
                yield db_id, doc_vector


class EmbeddingIndex:
    def __init__(self, vector_sz: int, buffer_size: int = 50000):
        self.buffer_size = buffer_size
        self.index_id_to_db_id = []
        vf_lists = 500
        self.index = faiss.index_factory(
            vector_sz,
            # "PCAR%d,IVF%d" % (pca_vector_sz, vf_lists),
            "IVF%d,SQ8" % (vf_lists),
            faiss.METRIC_INNER_PRODUCT
        )

    def index_data(self, vector_files: List[str]):
        start_time = time.time()
        buffer = []
        for i, item in enumerate(iterate_encoded_files(vector_files)):
            db_id, doc_vector = item
            buffer.append((db_id, doc_vector))
            if 0 < self.buffer_size == len(buffer):
                # indexing in batches is beneficial for many faiss index types
                self._index_batch(buffer)
                logger.info('data indexed %d, used_time: %f sec.',
                            len(self.index_id_to_db_id), time.time() - start_time)
                buffer = []
        self._index_batch(buffer)

        indexed_cnt = len(self.index_id_to_db_id)
        logger.info('Total data indexed %d', indexed_cnt)
        logger.info('Data indexing completed.')

    def train(self, files: List[str], train_size: int):
        train_per_file = int(train_size / len(files))

        print("Training with n vecs per file:", train_per_file)
        train_vecs = []
        for vec_file in tqdm.tqdm(files, desc="Collecting training data from files"):
            with open(vec_file, 'rb') as f:
                vecs = pickle.load(f)
                for v_i in np.random.choice(list(range(len(vecs))), train_per_file, replace=False):
                    train_vecs.append(vecs[v_i][1])

        data = np.array(train_vecs)

        print("Training, data size:", data.shape)
        self.index.train(data)
        print("Training finished")

    def _index_batch(self, data: List[Tuple[object, np.array]]):
        if len(data) == 0:
            return
        db_ids = [t[0] for t in data]
        vectors = [np.reshape(t[1], (1, -1)) for t in data]
        vectors = np.concatenate(vectors, axis=0)
        self._update_id_mapping(db_ids)
        self.index.add(vectors)

    def search_knn(self, query_vectors: np.array, top_docs: int) -> List[Tuple[List[str], List[float]]]:
        scores, indexes = self.index.search(query_vectors, top_docs)
        # convert to external ids
        db_ids = [[self.index_id_to_db_id[i] for i in query_top_idxs] for query_top_idxs in indexes]
        result = [(db_ids[i], scores[i]) for i in range(len(db_ids))]
        return result

    def serialize(self, file: str):
        logger.info('Serializing index to %s', file)

        if os.path.isdir(file):
            index_file = os.path.join(file, "index.dpr")
            meta_file = os.path.join(file, "index_meta.dpr")
        else:
            index_file = file + '.index.dpr'
            meta_file = file + '.index_meta.dpr'

        faiss.write_index(self.index, index_file)
        with open(meta_file, mode='wb') as f:
            pickle.dump(self.index_id_to_db_id, f)

    def deserialize_from(self, file: Path):
        logger.info('Loading index from %s', file)

        if file.is_dir():
            index_file = str(file / "index.dpr")
            meta_file = str(file / "index_meta.dpr")
        else:
            index_file = str(file) + '.index.dpr'
            meta_file = str(file) + '.index_meta.dpr'

        self.index = faiss.read_index(index_file)
        logger.info('Loaded index of type %s and size %d', type(self.index), self.index.ntotal)

        with open(meta_file, "rb") as reader:
            self.index_id_to_db_id = pickle.load(reader)
        assert len(
            self.index_id_to_db_id) == self.index.ntotal, 'Deserialized index_id_to_db_id should match faiss index size'

    def _update_id_mapping(self, db_ids: List):
        self.index_id_to_db_id.extend(db_ids)
