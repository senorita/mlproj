from typing import List

import attr

from bot.models.bert import Answerer, AnswererConfig
from bot.models.reranker import Reranker, RerankerConfig
from bot.retriever import Retriever, RetrieverConfig
from common.entities import Answer


@attr.s(auto_attribs=True)
class QABotConfig:
    answerer_config: AnswererConfig
    reranker_config: RerankerConfig
    retriever_config: RetrieverConfig


class QABot:
    def __init__(self, config: QABotConfig):
        self.retriever: Retriever = Retriever(config.retriever_config)
        self.reranker: Reranker = Reranker(config.reranker_config)
        self.answerer: Answerer = Answerer(config.answerer_config)

    def ask(self, question: str) -> List[Answer]:
        docs = self.retriever.retrieve(question)
        docs = self.reranker.rerank(question, docs)
        return self.answerer.answer(docs, question)
