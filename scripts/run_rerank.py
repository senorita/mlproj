import argparse
import json
import os
import sys
from pathlib import Path

sys.path.append(os.path.join(os.path.dirname(__file__), '..'))
from bot.models.reranker import RerankerConfig, Reranker
from common.entities import Document
from common.utils import structure

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--config_from_path', type=lambda x: (json.loads(Path(x).expanduser().read_text())), required=True)
    parsed = parser.parse_args()

    args = structure(parsed.config_from_path, RerankerConfig)

    reranker = Reranker(args)
    question = 'what is dog`s name?'
    docs = [
        Document('1', 'i like kettles', ''),
        Document('2', 'my dog climbs up the walls', ''),
        Document('3', 'dogs name is alan', ''),
        Document('4', 'bob eats his mustache', ''),
    ]

    print("init docs")
    for d in docs:
        print(d)
    print("reranked docs")
    for d in reranker.rerank(question, docs):
        print(d)
