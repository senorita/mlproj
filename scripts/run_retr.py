import argparse
import json
import os
import sys
from pathlib import Path

sys.path.append(os.path.join(os.path.dirname(__file__), '..'))
from bot.retriever import RetrieverConfig, Retriever
from common.utils import structure

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--config_from_path', type=lambda x: (json.loads(Path(x).expanduser().read_text())), required=True)
    parsed = parser.parse_args()
    args = structure(parsed.config_from_path, RetrieverConfig)
    retriever = Retriever(args)

    for d in retriever.retrieve('who is aaron?'):
        print(d)
