import argparse
import json
import os
import sys
from pathlib import Path

sys.path.append(os.path.join(os.path.dirname(__file__), '..'))
from common.utils import structure
from bot.bot import QABotConfig, QABot

try:
    import readline
except:
    pass

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--config_from_path', type=lambda x: (json.loads(Path(x).expanduser().read_text())), required=True)
    parsed = parser.parse_args()
    args = structure(parsed.config_from_path, QABotConfig)
    bot = QABot(args)

    while True:
        s = input('>')
        if not s:
            s = "Who is the elder brother of Moses?"
        anses = bot.ask(s)
        print([a.text for a in anses])
