import argparse
import json
import os
import sys
from pathlib import Path

sys.path.append(os.path.join(os.path.dirname(__file__), '..'))
from common.utils import structure
from bot.models import AnswererConfig, Answerer, Document

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--config_from_path', type=lambda x: (json.loads(Path(x).expanduser().read_text())), required=True)
    parsed = parser.parse_args()
    args = structure(parsed.config_from_path, AnswererConfig)
    answerer = Answerer(args)

    docs = [
        Document(
            id='1',
            text='Ms andersen (Aaron) was a cool guy, he had some high quality kettles.',
            title='Aaron'
        ),
    ]

    while True:
        anses = answerer.answer(docs, input())
        print([a.text for a in anses])
