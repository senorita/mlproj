FROM nvidia/cuda:11.0-base

RUN apt-get update && apt-get install -y software-properties-common \
    && add-apt-repository ppa:deadsnakes/ppa \
    && apt-get update \
    && apt-get install -y --no-install-recommends python3.6-dev pipenv

ADD Pipfile proj/Pipfile

WORKDIR proj

RUN pipenv install --skip-lock

ADD . .

EXPOSE 8888

ENTRYPOINT ["pipenv", "run", "python3", "app/app.py", "--bot_config", "app/bot_docker.json"]