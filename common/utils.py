import argparse
import json
import random
from pathlib import Path
from typing import Dict, Any

import numpy as np
import torch
from cattr import Converter


def structure(obj, cls):
    converter = Converter()
    converter.register_structure_hook(Path, lambda x, _: Path(x).expanduser().resolve())
    return converter.structure(obj, cls)


def set_seed(seed):
    random.seed(seed)
    np.random.seed(seed)
    torch.manual_seed(seed)


def get_encoder_checkpoint_params_names():
    return ['do_lower_case', 'pretrained_model_cfg', 'encoder_model_type',
            'pretrained_file',
            'projection_dim', 'sequence_length']


def set_encoder_params_from_state(state, args):
    if not state:
        return
    params_to_save = get_encoder_checkpoint_params_names()

    override_params = [(param, state[param]) for param in params_to_save if param in state and state[param]]
    for param, value in override_params:
        if hasattr(args, param):
            print('Overriding args parameter value from checkpoint state. Param = %s, value = %s', param, value)
        setattr(args, param, value)
    return args


def normalize(s: str) -> str:
    return ' '.join(s.strip().split())


def normalize_question(question: str) -> str:
    question = normalize(question)
    if question[-1] == '?':
        question = question[:-1]
    return question


def choose_device(use_cuda: bool):
    if not use_cuda:
        return torch.device('cpu')
    return torch.device('cuda:0')


def json_text(f: str):
    return json.loads(Path(f).expanduser().read_text())


def batch_to_device(batch: Dict[str, Any], device: torch.device):
    return {k: v.to(device) if type(v) == torch.Tensor else v for k, v in batch.items()}


def config_from_file(cls):
    def _parser(path: str):
        try:
            d = json.loads(Path(path).expanduser().resolve().read_text())
            return structure(d, cls)
        except Exception as e:
            print(e)
            raise argparse.ArgumentTypeError(e)

    return _parser
