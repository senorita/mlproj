from typing import Optional, List

import attr


@attr.s(auto_attribs=True)
class Document:
    id: str
    text: str
    title: str
    score: Optional[float] = attr.ib(default=0.0)

    def with_score(self, score: float):
        return attr.evolve(self, score=score)


@attr.s(auto_attribs=True)
class Span:
    start: int
    end: int
    context: Document
    score: float


@attr.s(auto_attribs=True)
class Answer:
    text: str
    evidence: List[Span]
