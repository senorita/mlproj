import argparse
import csv
import os
import sys
from pathlib import Path

import tqdm

sys.path.append(os.path.join(os.path.dirname(__file__), '..'))
from common.entities import Document
from db.client import DocumentIndex

INDEX_NAME = 'docs'


def read_passages(passages_path: Path):
    with passages_path.open('r') as f:
        reader = csv.reader(f, delimiter="\t")
        for row in tqdm.tqdm(reader, total=21015325):
            yield Document(
                id=row[0],
                text=row[1],
                title=row[2],
            )


def main(args: argparse.Namespace):
    elastic_address: str = args.elastic_address
    indexer = DocumentIndex(elastic_address)
    passages_path: Path = args.passages_path
    indexer.reset_index()
    indexer.index_doc_stream(read_passages(passages_path))
    print("Docs in index:", indexer.count())


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--elastic_address', type=str, required=True)
    parser.add_argument('--passages_path', type=Path, required=True)
    main(parser.parse_args())
