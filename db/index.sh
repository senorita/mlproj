docker-compose up -d

sleep 10 # Wait until elastic initializes.
python3 indexer.py --elastic_address localhost --passages_path ../.data/wiki/psgs_w100.tsv

docker-compose down
