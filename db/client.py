from typing import List, Iterable

from elasticsearch import Elasticsearch
from elasticsearch.helpers import parallel_bulk
from elasticsearch_dsl import token_filter, analyzer

from common.entities import Document

INDEX_NAME = 'docs'

stop_word_filter = token_filter('stop_word_filter', type='stop', stopwords='_english_')
stemmer = token_filter('my-stemmer', type='stemmer', name="light_english")
doc_analyzer = analyzer('my-analyzer', tokenizer='standard', filter=['lowercase', stop_word_filter, stemmer])


def hits_to_docs(res):
    docs: List[Document] = []
    for hit in res['hits']['hits']:
        docs.append(Document(
            id=hit['_id'],
            title=hit['_source']['title'],
            text=hit['_source']['text'],
            score=hit['_score'],
        ))
    return docs


class DocumentIndex:
    def __init__(self, host, index_name=INDEX_NAME):
        self.client = Elasticsearch(hosts=[host])
        self.index_name = index_name

    def reset_index(self):
        self.client.indices.delete(self.index_name, ignore=404)
        self.client.indices.create(index=self.index_name, body={
            "settings": {
                "analysis": {
                    "analyzer": {
                        "my-analyzer": doc_analyzer.get_definition(),
                    },
                    "filter": {
                        "stop_word_filter": stop_word_filter.get_definition(),
                        "my-stemmer": stemmer.get_definition(),
                    }
                }
            },
            "mappings": {
                "properties": {
                    "title": {
                        "type": "text",
                        "analyzer": "my-analyzer"
                    },
                    "text": {
                        "type": "text",
                        "analyzer": "my-analyzer"
                    }
                }
            }
        }, ignore=400)

    def retrieve(self, question: str, size: int = 10) -> List[Document]:
        res = self.client.search(body={
            'query': {
                'multi_match': {
                    'type': 'cross_fields',
                    'query': question,
                    'fields': ['text', 'title'],
                },
            },
            'size': size,
        }, index=self.index_name)
        return hits_to_docs(res)

    def get_docs(self, doc_ids: List[str]) -> List[Document]:
        return [self.get_doc(doc_id) for doc_id in doc_ids]

    def get_doc(self, doc_id: str) -> Document:
        res = self.client.search(body={
            'query': {
                'match': {
                    '_id': doc_id,
                }
            },
        }, index=self.index_name)
        docs = hits_to_docs(res)
        if not docs:
            raise Exception(f'Document not found: {doc_id}')
        assert len(docs) == 1
        return docs[0]

    def _convert_doc_to_action(self, doc: Document):
        return {
            '_id': doc.id,
            '_index': self.index_name,
            "text": doc.text,
            "title": doc.title,
        }

    def index_doc_stream(self, docs: Iterable[Document]):
        for r in parallel_bulk(self.client, (self._convert_doc_to_action(d) for d in docs), thread_count=8):
            pass

    def count(self):
        return self.client.count(index=self.index_name)
