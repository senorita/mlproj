import argparse
import json
import os
import sys
import typing
from pathlib import Path

import tornado
import tornado.httpserver
import tornado.ioloop
import tornado.web
from cattr import unstructure

sys.path.append(os.path.join(os.path.dirname(__file__), '..'))

from bot.bot import QABot, QABotConfig
from common.utils import structure


class IndexHandler(tornado.web.RequestHandler):
    def get(self):
        self.render("index.html")


T = typing.TypeVar("T")


class LazyInit(typing.Generic[T]):
    def __init__(self, constructor: typing.Callable[[], T]):
        print('New proxy')
        self.constructor = constructor
        self.obj: typing.Optional[T] = None

    def get(self) -> T:
        if not self.obj:
            self.obj = self.constructor()
        assert self.obj
        return self.obj


class QaHandler(tornado.web.RequestHandler):
    def initialize(self, delegate: LazyInit[QABot]) -> None:
        self.bot_delegate = delegate

    def get(self):
        q = self.get_argument("q", "")

        bot = self.bot_delegate.get()
        answers = bot.ask(q)

        self.write(json.dumps({
            "answers": unstructure(answers)
        }))


def main(args: argparse.Namespace):
    bot_config = structure(args.bot_config, QABotConfig)

    app = tornado.web.Application([
        ('/', IndexHandler),
        (r"/static/(.*)", tornado.web.StaticFileHandler, {"path": "app/static"}),
        ('/ask', QaHandler, {'delegate': LazyInit(lambda: QABot(bot_config))}),
    ], debug=True)
    http_server = tornado.httpserver.HTTPServer(app)
    http_server.listen(8888)
    tornado.ioloop.IOLoop.current().start()


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--bot_config', type=lambda x: (json.loads(Path(x).expanduser().read_text())), required=True)
    main(parser.parse_args())
