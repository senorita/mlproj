function onFormSubmit() {
    ask()
    return false
}

function makeSpan(answer, ev) {
    const window = 100
    let evSpanCont = $(document.createElement('div'))

    let evPreSpan = $(document.createElement('span'))
        .text(ev.context.text.slice(
            Math.max(0, ev.start - window), ev.start
        ))
    let evSpan = $(document.createElement('span'))
        .text(ev.context.text.slice(
            ev.start, ev.start + answer.text.length + 1)
        )
        .css('color', 'red')
    let evPostSpan = $(document.createElement('span'))
        .text(ev.context.text.slice(
            ev.start + answer.text.length + 1,
            Math.min(ev.start + answer.text.length + 1 + 100, ev.context.text.length),
        ))
    evSpanCont.append(evPreSpan, evSpan, evPostSpan)
    return evSpanCont
}

function makeAnswer(a) {
    let card = $(document.createElement('div')).addClass('card');
    let cardBody = $(document.createElement('div')).addClass('card-body')
    cardBody.text(a.text)
    let evList = $(document.createElement('ul'))
    let evEls = a.evidence.map((ev) => {
        let wiki_title = ev.context.title
        let wiki_link = `https://en.wikipedia.org/wiki/${wiki_title}`
        let evSpan = makeSpan(a, ev)
        let evCard = $(document.createElement('li'))
        let evLink = $(document.createElement('a'))
            .text(wiki_title).attr('target', '_blank').attr('href', wiki_link)
        let evProb = $(document.createElement('span')).text(` (${Math.round(ev.score * 100) / 100})`)
        evCard.append(evSpan, evLink, evProb)
        return evCard
    })
    evList.append(evEls)

    card.append(cardBody, evList)
    return card;
}

function ask() {
    let q = document.getElementById("question").value
    console.log("Asking:", q)
    fetch(`/ask?q=${q}`)
        .then((value) => value.json())
        .then((value) => {
            displayAnswers(value.answers)
        })
}

function displayAnswers(answers) {
    console.log('Answered:', answers)
    let els = answers.map((v) => makeAnswer(v));
    $('#answers-list').empty()
    $('#answers-list').append(els)

    console.log('Render:', els)
}

